﻿namespace tmh306ödev
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dosyaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.açToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kaydetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.çıkışToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.düzenleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.averageOrtalamaYöntemiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bT709YöntemiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.açıklıkDesaturationYöntemiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renkKanalıYöntemiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.normalizeRenkKanalıYöntemiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dosyaToolStripMenuItem,
            this.düzenleToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dosyaToolStripMenuItem
            // 
            this.dosyaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.açToolStripMenuItem,
            this.kaydetToolStripMenuItem,
            this.çıkışToolStripMenuItem});
            this.dosyaToolStripMenuItem.Name = "dosyaToolStripMenuItem";
            this.dosyaToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.dosyaToolStripMenuItem.Text = "Dosya";
            // 
            // açToolStripMenuItem
            // 
            this.açToolStripMenuItem.Name = "açToolStripMenuItem";
            this.açToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.açToolStripMenuItem.Text = "Aç";
            this.açToolStripMenuItem.Click += new System.EventHandler(this.açToolStripMenuItem_Click);
            // 
            // kaydetToolStripMenuItem
            // 
            this.kaydetToolStripMenuItem.Name = "kaydetToolStripMenuItem";
            this.kaydetToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.kaydetToolStripMenuItem.Text = "Kaydet";
            this.kaydetToolStripMenuItem.Click += new System.EventHandler(this.kaydetToolStripMenuItem_Click);
            // 
            // çıkışToolStripMenuItem
            // 
            this.çıkışToolStripMenuItem.Name = "çıkışToolStripMenuItem";
            this.çıkışToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.çıkışToolStripMenuItem.Text = "Çıkış";
            this.çıkışToolStripMenuItem.Click += new System.EventHandler(this.çıkışToolStripMenuItem_Click);
            // 
            // düzenleToolStripMenuItem
            // 
            this.düzenleToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.averageOrtalamaYöntemiToolStripMenuItem,
            this.bT709YöntemiToolStripMenuItem,
            this.açıklıkDesaturationYöntemiToolStripMenuItem,
            this.renkKanalıYöntemiToolStripMenuItem,
            this.normalizeRenkKanalıYöntemiToolStripMenuItem});
            this.düzenleToolStripMenuItem.Name = "düzenleToolStripMenuItem";
            this.düzenleToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.düzenleToolStripMenuItem.Text = "Düzenle";
            // 
            // averageOrtalamaYöntemiToolStripMenuItem
            // 
            this.averageOrtalamaYöntemiToolStripMenuItem.Name = "averageOrtalamaYöntemiToolStripMenuItem";
            this.averageOrtalamaYöntemiToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.averageOrtalamaYöntemiToolStripMenuItem.Text = "Average(Ortalama) Yöntemi";
            this.averageOrtalamaYöntemiToolStripMenuItem.Click += new System.EventHandler(this.averageOrtalamaYöntemiToolStripMenuItem_Click);
            // 
            // bT709YöntemiToolStripMenuItem
            // 
            this.bT709YöntemiToolStripMenuItem.Name = "bT709YöntemiToolStripMenuItem";
            this.bT709YöntemiToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.bT709YöntemiToolStripMenuItem.Text = "BT-709 Yöntemi";
            this.bT709YöntemiToolStripMenuItem.Click += new System.EventHandler(this.bT709YöntemiToolStripMenuItem_Click);
            // 
            // açıklıkDesaturationYöntemiToolStripMenuItem
            // 
            this.açıklıkDesaturationYöntemiToolStripMenuItem.Name = "açıklıkDesaturationYöntemiToolStripMenuItem";
            this.açıklıkDesaturationYöntemiToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.açıklıkDesaturationYöntemiToolStripMenuItem.Text = "Açıklık(Desaturation) Yöntemi";
            this.açıklıkDesaturationYöntemiToolStripMenuItem.Click += new System.EventHandler(this.açıklıkDesaturationYöntemiToolStripMenuItem_Click);
            // 
            // renkKanalıYöntemiToolStripMenuItem
            // 
            this.renkKanalıYöntemiToolStripMenuItem.Name = "renkKanalıYöntemiToolStripMenuItem";
            this.renkKanalıYöntemiToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.renkKanalıYöntemiToolStripMenuItem.Text = "Renk Kanalı Yöntemi";
            this.renkKanalıYöntemiToolStripMenuItem.Click += new System.EventHandler(this.renkKanalıYöntemiToolStripMenuItem_Click);
            // 
            // normalizeRenkKanalıYöntemiToolStripMenuItem
            // 
            this.normalizeRenkKanalıYöntemiToolStripMenuItem.Name = "normalizeRenkKanalıYöntemiToolStripMenuItem";
            this.normalizeRenkKanalıYöntemiToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.normalizeRenkKanalıYöntemiToolStripMenuItem.Text = "Normalize Renk Kanalı Yöntemi";
            this.normalizeRenkKanalıYöntemiToolStripMenuItem.Click += new System.EventHandler(this.normalizeRenkKanalıYöntemiToolStripMenuItem_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(21, 28);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(295, 207);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(373, 28);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(289, 207);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(100, 241);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(469, 241);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MenuStrip menuStrip1;
        private ToolStripMenuItem dosyaToolStripMenuItem;
        private ToolStripMenuItem açToolStripMenuItem;
        private ToolStripMenuItem kaydetToolStripMenuItem;
        private ToolStripMenuItem çıkışToolStripMenuItem;
        private ToolStripMenuItem düzenleToolStripMenuItem;
        private ToolStripMenuItem averageOrtalamaYöntemiToolStripMenuItem;
        private ToolStripMenuItem bT709YöntemiToolStripMenuItem;
        private ToolStripMenuItem açıklıkDesaturationYöntemiToolStripMenuItem;
        private ToolStripMenuItem renkKanalıYöntemiToolStripMenuItem;
        private ToolStripMenuItem normalizeRenkKanalıYöntemiToolStripMenuItem;
        private PictureBox pictureBox1;
        private PictureBox pictureBox2;
        private Button button1;
        private Button button2;
    }
}